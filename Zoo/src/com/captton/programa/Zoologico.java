package com.captton.programa;

import com.captton.zoologico.Pajaro;
import com.captton.zoologico.Perro;
import com.captton.zoologico.Pez;

public class Zoologico {

	public static void main(String[] args) {

		Perro perr1 = new Perro(5.0f,"Puchi",10.0f);
		System.out.println(perr1.getNombre());
		System.out.println("Edad "+perr1.getEdad());
		System.out.println(perr1.Accion());
		System.out.println("Edad luego de accion "+perr1.getEdad());
		System.out.println("Peso "+perr1.getPeso());
		perr1.Comen(500.0f);
		System.out.println("Peso luego de comer "+perr1.getPeso());
		
		System.out.println("\n");
		
		Pajaro paj1 = new Pajaro(3.0f,"Piolin",2.0f);
		System.out.println(paj1.getNombre());
		System.out.println("Edad "+paj1.getEdad());
		System.out.println(paj1.Accion());
		System.out.println("Edad luego de accion "+paj1.getEdad());
		System.out.println("Peso "+paj1.getPeso());
		paj1.Comen(30.0f);
		System.out.println("Peso luego de comer "+paj1.getPeso());
		
		System.out.println("\n");
		
		Pez pez1 = new Pez(1.0f,"tiburoncin",500.0f);
		System.out.println(pez1.getNombre());
		System.out.println("Edad "+pez1.getEdad());
		System.out.println(pez1.Accion());
		System.out.println("Edad luego de accion "+pez1.getEdad());
		System.out.println("Peso "+pez1.getPeso());
		pez1.Comen(100.0f);
		System.out.println("Peso luego de comer "+pez1.getPeso());

		
	}

}
