package com.captton.zoologico;

public class Pez extends Zoo {

	public Pez(float edad, String nombre, float peso) {
		super(edad, nombre, peso);
		// TODO Auto-generated constructor stub
	}

	public Pez() {
		// TODO Auto-generated constructor stub
	}

	public String Accion() {		//Accion nadar
		this.Rejuvenecer();
		return "Que lindo es nadar";
	}

	public float Comen(float cant) { 	//cant = cant de alimento ingerido
		float pesoF = this.getPeso() + (cant * 0.7f);
		this.setPeso(pesoF);
		return pesoF;
	}
}
