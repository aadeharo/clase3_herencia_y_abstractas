package com.captton.zoologico;

public class Pajaro extends Zoo {

	public Pajaro(float edad, String nombre, float peso) {
		super(edad, nombre, peso);

	}

	public Pajaro() {

	}
	
	public String Accion() {		//Accion volar
		this.Rejuvenecer();
		return "Desde Arriba todo es bello!";
	}

	public float Comen(float cant) {		//cant = cant de alimento ingerido
		float pesoF = this.getPeso() + (cant * 0.3f);
		this.setPeso(pesoF);
		return pesoF;
	}

}
