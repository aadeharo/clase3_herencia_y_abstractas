package com.captton.zoologico;

public abstract class Zoo {
	
	protected float edad;
	protected String nombre;
	protected float peso;
	
	public Zoo(float edad, String nombre, float peso) {
		super();
		this.edad = edad;
		this.nombre = nombre;
		this.peso = peso;
	}

	public Zoo() {
		super();
	}

	public float getEdad() {
		return edad;
	}

	public void setEdad(float edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	public abstract float Comen(float cant);		//cant = cant de alimento ingerido
	
	public final float Rejuvenecer() {		//Metodo final, todos los animales rejuvenecen 10% cuando realizan acciones volar, nadar, ladrar
		return edad -= 0.1f;				//Rejuvenece 10% a todos por igual
	}
	
	
	
	
	
	
	
	
	
	

}
