package com.captton.zoologico;

public class Perro extends Zoo {

	public Perro(float edad, String nombre, float peso) {
		super(edad, nombre, peso);

	}

	public Perro() {

	}

	public String Accion() {		//Accion ladrar
		this.Rejuvenecer();
		return "Me relaja ladrar";
	}

	public float Comen(float cant) {		//cant = cant de alimento ingerido
		float pesoF = this.getPeso() + (cant * 0.5f);
		this.setPeso(pesoF);
		return pesoF;
	}
	
	

}
